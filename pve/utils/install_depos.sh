#!/bin/bash

orga=evolix
repos=("pve-docs" "pve-drbd" "pve-drbd" "pve-storage" "pve-manager")

cd /tmp/

mkdir uploadfiles
for repo in "${repos[@]}"; do
    git clone "https://gitea.evolix.org/${orga}/${repo}.git"
    cd ${repo}
    make dinstall || true
    mv --force *.deb /tmp/uploadfiles/
    mv --force *.changes /tmp/uploadfiles/
    mv --force *.buildinfo /tmp/uploadfiles/
    cd ..
done

